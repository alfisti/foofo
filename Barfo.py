from Foofo import Foofo


_is_debug = True
_lol = 2046


def _lol_fn(_x):
    return _x + 1224


class Barfo(Foofo):

    def __init__(self,
                 x,
                 y,
                 k,):

        super().__init__(
            x=x,
            y=y,

            # external foofos should be raw attrs
            # this works currently only because brafo can freeze on its own...
            # TODO: external foofos spawned in dyn attrs
            brafo=Brafo(k),

            fget_map={
                'z': lambda: 10,
                'p': lambda: self.x * 2 + self.y + 1,
                'q': self._q,
                'r_lamb': self._r_lamb,
                'r_fn': self._r_fn,
                's': self._s,
                's_lamb': self._s_lamb,
                'brafo_k': lambda: self.brafo.k,
            },
            is_debug=_is_debug
        )

    # usage: obj.q

    def _q(self):
        return self.p * 5 + self.x + 2

    # usage: obj.r_lamb(a)

    def _r_lamb(self):
        return lambda _a: self.q + _a

    def _r_fn(self):

        def _fn(_a):
            return self.q + _a

        return _fn

    def _s(self):
        return self.r_lamb(_lol_fn(_lol))

    def _s_lamb(self):
        return lambda _a: self.r_lamb(_lol_fn(_lol)) + _a


class Brafo(Foofo):

    def __init__(self,
                 k):

        super().__init__(k=k, is_debug=_is_debug)


if __name__ == '__main__':

    bar = Barfo(x=5, y=6, k=20)

    assert not bar.dep_struct.is_frozen

    assert bar.is_attr_raw('x')
    assert bar.is_attr_raw('y')
    assert bar.is_attr_raw('brafo')
    assert bar.is_attr_raw('z')
    assert not bar.is_attr_raw('p')
    assert not bar.is_attr_raw('q')
    assert not bar.is_attr_raw('r_lamb')
    assert not bar.is_attr_raw('r_fn')
    assert not bar.is_attr_raw('s')
    assert not bar.is_attr_raw('s_lamb')
    assert not bar.is_attr_raw('brafo_k')

    assert not bar.is_attr_dormant('x')
    assert not bar.is_attr_dormant('y')
    assert not bar.is_attr_dormant('brafo')
    assert not bar.is_attr_dormant('z')
    assert bar.is_attr_dormant('p')
    assert bar.is_attr_dormant('q')
    assert bar.is_attr_dormant('r_lamb')
    assert bar.is_attr_dormant('r_fn')
    assert bar.is_attr_dormant('s')
    assert bar.is_attr_dormant('s_lamb')
    assert bar.is_attr_dormant('brafo_k')

    # no evaluation required for raw attrs
    assert bar.x == 5
    assert bar.y == 6
    assert bar.z == 10

    assert not bar.dep_struct.is_frozen

    assert not bar.is_attr_dormant('x')
    assert not bar.is_attr_dormant('y')
    assert not bar.is_attr_dormant('brafo')
    assert not bar.is_attr_dormant('z')
    assert bar.is_attr_dormant('p')
    assert bar.is_attr_dormant('q')
    assert bar.is_attr_dormant('r_lamb')
    assert bar.is_attr_dormant('r_fn')
    assert bar.is_attr_dormant('s')
    assert bar.is_attr_dormant('s_lamb')
    assert bar.is_attr_dormant('brafo_k')

    # dyn attrs only depending on raw attrs
    assert bar.p == 17

    # first dyn-get freezes the foofo
    assert bar.dep_struct.is_frozen

    assert not bar.is_attr_dormant('x')
    assert not bar.is_attr_dormant('y')
    assert not bar.is_attr_dormant('brafo')
    assert not bar.is_attr_dormant('z')
    assert not bar.is_attr_dormant('p')
    assert bar.is_attr_dormant('q')
    assert bar.is_attr_dormant('r_lamb')
    assert bar.is_attr_dormant('r_fn')
    assert bar.is_attr_dormant('s')
    assert bar.is_attr_dormant('s_lamb')
    assert bar.is_attr_dormant('brafo_k')

    # dyn attrs depending on dormant dyn attrs
    assert bar.s == 3362

    assert not bar.is_attr_dormant('x')
    assert not bar.is_attr_dormant('y')
    assert not bar.is_attr_dormant('brafo')
    assert not bar.is_attr_dormant('z')
    assert not bar.is_attr_dormant('p')
    assert not bar.is_attr_dormant('q')
    assert not bar.is_attr_dormant('r_lamb')
    assert bar.is_attr_dormant('r_fn')
    assert not bar.is_attr_dormant('s')
    assert bar.is_attr_dormant('s_lamb')
    assert bar.is_attr_dormant('brafo_k')

    assert bar.q == 92
    assert bar.r_lamb(2) == 94
    assert bar.r_fn(3) == 95
    assert bar.s_lamb(4) == 3366
    assert bar.brafo_k == 20

    assert not bar.is_attr_dormant('x')
    assert not bar.is_attr_dormant('y')
    assert not bar.is_attr_dormant('brafo')
    assert not bar.is_attr_dormant('z')
    assert not bar.is_attr_dormant('p')
    assert not bar.is_attr_dormant('q')
    assert not bar.is_attr_dormant('r_lamb')
    assert not bar.is_attr_dormant('r_fn')
    assert not bar.is_attr_dormant('s')
    assert not bar.is_attr_dormant('s_lamb')
    assert not bar.is_attr_dormant('brafo_k')

    bar.q = 66

    assert not bar.is_attr_dormant('x')
    assert not bar.is_attr_dormant('y')
    assert not bar.is_attr_dormant('brafo')
    assert not bar.is_attr_dormant('z')
    assert not bar.is_attr_dormant('p')
    assert not bar.is_attr_dormant('q')
    assert bar.is_attr_dormant('r_lamb')
    assert bar.is_attr_dormant('r_fn')
    assert bar.is_attr_dormant('s')
    assert bar.is_attr_dormant('s_lamb')
    assert not bar.is_attr_dormant('brafo_k')

    assert bar.x == 5
    assert bar.y == 6
    assert bar.z == 10
    assert bar.p == 17
    assert bar.q == 66
    assert bar.r_lamb(2) == 68
    assert bar.r_fn(3) == 69
    assert bar.s == 3336
    assert bar.s_lamb(4) == 3340
    assert bar.brafo_k == 20

    del bar.p

    assert not bar.is_attr_dormant('x')
    assert not bar.is_attr_dormant('y')
    assert not bar.is_attr_dormant('brafo')
    assert not bar.is_attr_dormant('z')
    assert bar.is_attr_dormant('p')
    assert bar.is_attr_dormant('q')
    assert bar.is_attr_dormant('r_lamb')
    assert bar.is_attr_dormant('r_fn')
    assert bar.is_attr_dormant('s')
    assert bar.is_attr_dormant('s_lamb')
    assert not bar.is_attr_dormant('brafo_k')

    assert bar.x == 5
    assert bar.y == 6
    assert bar.z == 10

    assert not bar.is_attr_dormant('x')
    assert not bar.is_attr_dormant('y')
    assert not bar.is_attr_dormant('brafo')
    assert not bar.is_attr_dormant('z')
    assert bar.is_attr_dormant('p')
    assert bar.is_attr_dormant('q')
    assert bar.is_attr_dormant('r_lamb')
    assert bar.is_attr_dormant('r_fn')
    assert bar.is_attr_dormant('s')
    assert bar.is_attr_dormant('s_lamb')
    assert not bar.is_attr_dormant('brafo_k')

    assert bar.p == 17
    assert bar.q == 92
    assert bar.r_lamb(2) == 94
    assert bar.r_fn(3) == 95
    assert bar.s == 3362
    assert bar.s_lamb(4) == 3366
    assert bar.brafo_k == 20

    brafo_new = Brafo(k=23)

    bar.brafo = brafo_new

    pass
