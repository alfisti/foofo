import dis

from platform import python_implementation as pimp


class Foofo:

    def __init__(self,
                 is_debug=False,
                 fget_map=None,
                 **attrs_raw):

        # 'dis' and 'id()' is only guaranteed to work with CPython
        if pimp() != 'CPython':
            raise Exception

        self.__is_debug = is_debug

        self.__mem_id = id(self)
        self.__repr = '{}({})'.format(self.__class__.__name__, self.__mem_id)

        if len(attrs_raw) == 0:
            raise Exception

        keys = list(attrs_raw.keys())
        if fget_map is not None:
            keys.extend(fget_map.keys())

        if len(set(keys)) != len(keys):
            raise Exception

        # initialise all attr-ptrs, but identify external foofos later
        self.__key_ptrs = {
            key: _AttributePointer(foofo=self, key=key)
            for key in keys
        }
        self.__keys_to_external_foofos = set()

        # keys in public; vals as key ptrs
        self.__dep_struct = _DepStruct(foofo=self, keys=keys)

        for key, val in attrs_raw.items():
            self._def_attr(key, is_raw=True, val_init=val)

        if fget_map is not None:

            if type(fget_map) != dict:
                raise Exception

            for key, fget in fget_map.items():
                self._def_attr(key, is_raw=False, fget=fget)

    # -------------------------------- public -------------------------------- #

    # iterate without evaluating

    @property
    def keys(self):
        return self.__key_ptrs.keys()

    @property
    def values(self):
        return map(lambda ptr: ptr.get_attr_private(), self.__key_ptrs.values())

    @property
    def items(self):
        return zip(self.keys, self.values)

    def __getitem__(self,
                    key):

        if key not in self.__key_ptrs.keys():
            raise Exception

        return self.__key_ptrs[key].get_attr_private()

    def __len__(self):
        return len(self.__dep_struct)

    @property
    def mem_id(self):
        return self.__mem_id

    @property
    def dep_struct(self):
        return self.__dep_struct

    def debug(self,
              is_debug):

        if type(is_debug) != bool:
            raise Exception

        self.__is_debug = is_debug

    def is_attr_raw(self,
                    key):

        if key not in self.__key_ptrs.keys():
            raise Exception

        return self.__key_ptrs[key].is_attr_raw

    def is_attr_dormant(self,
                        key):

        if key not in self.__key_ptrs.keys():
            raise Exception

        return self.__key_ptrs[key].is_attr_dormant

    @property
    def keys_to_external_foofos(self):
        return self.__keys_to_external_foofos

    def print(self,
              str_formattable,
              *args):

        if self.__is_debug:
            print(str_formattable.format(*args))

    def __repr__(self):
        return self.__repr

    # ------------------------------- private ------------------------------- #

    def _def_attr(self,
                  key,
                  is_raw=False,
                  val_init=None,
                  fget=None):

        # no on-the-fly attr definitions
        if self.__dep_struct.is_frozen:
            raise Exception

        # TODO: prevent attr-overwrite
        key_ptr = self.__key_ptrs[key]

        # double-check...
        if hasattr(self, key) or hasattr(self, key_ptr.key_private):
            raise Exception

        if is_raw:

            if val_init is None or type(val_init) == _DormantAttribute:
                raise Exception

            if Foofo in val_init.__class__.__mro__:
                self.__keys_to_external_foofos.add(key_ptr.key)

            deps_ptr = set()

        else:

            if fget is None:
                raise Exception

            deps_ptr = self._dep_search(fget_co=fget.__code__)

            val_init = (
                fget()
                if len(deps_ptr) == 0
                else _DormantAttribute(self.__mem_id, key)
            )

            # external foofos should only be loaded in raw attrs
            if Foofo in val_init.__class__.__mro__:
                raise Exception

        key_ptr.set_attr_private(val_init)

        # the holy trinity of OOP - getter, setter and deleter

        def fget_recursive(_self):

            # let's just treat '_self' as a dummy here.
            # sequence matters when f-getting.

            if key_ptr.is_attr_dormant:

                # fget is None for raw attrs, but they should never reach here.
                if fget is None:
                    raise Exception

                if not self.__dep_struct.is_frozen:

                    # freeze upon first dyn-get
                    self.__dep_struct.freeze()

                    # play it safe...
                    if not self.__dep_struct.is_frozen:
                        raise Exception

                # f-get dependencies
                for d_ptr in self.__dep_struct[key]:
                    d_ptr.get_attr()

                val = fget()
                self.print('F-get: {} = {}', key_ptr, val)
                key_ptr.set_attr_private(val)

            else:
                # raw attrs should never be dormant
                val = key_ptr.get_attr_private()
                self.print('Simple get: {} = {}', key_ptr, val)

            return val

        def fset(_self, val):

            # let's just treat '_self' as a dummy here.
            # sequence does not matter when deleting.

            for dep_ptr in self.__dep_struct.search_dependants(key):

                self.print('Delete {} due to {}', dep_ptr, key_ptr)
                dep_ptr.del_attr()

            if type(val) == _DormantAttribute:
                self.print('Delete {}', key_ptr)

            else:
                self.print('Set {} = {}', key_ptr, val)

            key_ptr.set_attr_private(val)

        def fdel(_self):

            # not deleting literally; just put it to sleep
            # note: some non-raw attributes can be indelible

            if len(self.__dep_struct[key]) == 0:
                raise Exception

            fset(_self, _DormantAttribute(self.__mem_id, key))

        setattr(Foofo, key, property(
            fget=fget_recursive,
            fset=fset,
            fdel=fdel,
        ))

        # should be initialised but not filled
        if key not in self.__dep_struct.keys() \
                or len(self.__dep_struct[key]) != 0:
            raise Exception

        self.__dep_struct[key] = deps_ptr

    def _dep_search(self,
                    fget_co,
                    level=0):

        # 'fget.__code__.co_names' will not suffice.
        # - (lambda) fn in (lambda) fn?
        # - global vars?
        # - etc.

        deps = set()
        tmp_foofo = None
        chevron = '>' * (level + 1)

        naughty_fns = {
            # no dynamic get/set/delete
            'getattr',
            'setattr',
            'delattr',

            # the most dangerous among all
            'eval',
            'exec',
        }

        for instr in dis.get_instructions(fget_co):

            self.print('{} {}'.format(chevron, instr))

            if instr.opname == 'LOAD_GLOBAL' and instr.argval in naughty_fns:
                raise Exception

            if tmp_foofo is not None:

                # referencing a foofo (e.g. self)...

                if instr.opname == 'LOAD_ATTR' or instr.opname == 'LOAD_METHOD':

                    key_ptr = _AttributePointer(
                        foofo=tmp_foofo,
                        key=instr.argval
                    )

                    self.print('{} Spotted \'{}\'', chevron, key_ptr)

                    # do NOT get dyn attr (i.e. eval) at this moment

                    if instr.argval in tmp_foofo.keys_to_external_foofos:

                        # external foofos should already be loaded in raw attrs
                        tmp_foofo = key_ptr.get_attr_private()

                    elif instr.argval in tmp_foofo.keys:

                        # end of get-chain
                        deps.add(key_ptr)
                        tmp_foofo = None

                    elif key_ptr.has_attr():

                        # TODO: check if genuinely 'authentic'
                        # non-foofo-attr
                        tmp_foofo = None

                    else:
                        raise Exception

                elif instr.opname == 'STORE_ATTR':
                    # do not allow self-setting attributes
                    raise Exception

                elif instr.opname == 'DELETE_ATTR':
                    # do not allow self-deleting attributes
                    raise Exception

                else:
                    raise Exception

            elif instr.opname == 'LOAD_DEREF' or instr.opname == 'LOAD_FAST':

                # can relax for 'this', 'cls'
                if instr.argval == 'self':

                    self.print('{} Attention...', chevron)
                    tmp_foofo = self

            elif instr.opname == 'LOAD_CONST' \
                    and type(instr.argval).__name__ == 'code':

                self.print('{} Spotted fn...', chevron)

                deps.update(self._dep_search(
                    fget_co=instr.argval,
                    level=level + 1,
                ))

        self.print('{} Deps: {}', chevron, deps)

        return deps


class _DepStruct(dict):

    def __init__(self,
                 foofo,
                 keys):

        # key-val pairs where val is set of immediate dependencies for key
        # keys in public and vals as attr-ptrs

        if Foofo not in foofo.__class__.__mro__:
            raise Exception

        super().__init__()

        self.__foofo = foofo
        self.__is_frozen = False
        self.__is_freezing = False
        self.__deps_frozen = dict()
        self.__dependants_frozen = dict()

        for key in keys:
            self[key] = set()

        self.__repr = '{}.DepStruct'.format(self.__foofo)

    def __setitem__(self,
                    key,
                    deps_immediate):

        if self.__is_frozen:
            raise Exception

        if type(deps_immediate) != set:
            raise Exception

        super().__setitem__(key, deps_immediate)

    def freeze(self):

        # forms entire tree then freeze
        self.__is_freezing = True
        self.__foofo.print('Freezing \'{}\'...'.format(self))

        # 'self.search' builds the graph
        # checks circular dep as well
        if any(_AttributePointer(foofo=self.__foofo, key=key)
               in self.search(key)
               for key in self.keys()):
            raise Exception

        self.__is_freezing = False
        self.__is_frozen = True

    @property
    def is_frozen(self):
        return self.__is_frozen

    def search(self,
               key):

        if key in self.__deps_frozen.keys():

            return self.__deps_frozen[key]

        # key is public; deps are attr-ptrs
        deps_searched = set()
        deps_backlog = self[key].copy()

        if len(deps_backlog) > 0:

            while True:

                dep_to_search = deps_backlog.pop()
                deps_searched.add(dep_to_search)

                foofo = dep_to_search.foofo

                if foofo.is_attr_raw(dep_to_search.key):
                    deps_backlog_new = set()

                else:
                    if foofo != self.__foofo:
                        foofo.dep_struct.freeze()

                    deps_backlog_new = foofo.dep_struct.search(
                        dep_to_search.key
                    )

                if len(deps_backlog) == 0 \
                        and deps_backlog_new.issubset(deps_searched):
                    break

                deps_backlog.update(deps_backlog_new)

        self.__foofo.print('Searched deps of \'{}.{}\'', self.__foofo, key)

        if self.__is_freezing:
            self.__deps_frozen[key] = deps_searched

        return deps_searched

    def search_dependants(self,
                          key):

        if not self.__is_frozen:
            raise Exception

        if key not in self.__deps_frozen.keys():
            raise Exception

        if key in self.__dependants_frozen.keys():

            return self.__dependants_frozen[key]

        deps = {
            _AttributePointer(foofo=self.__foofo, key=dep)
            for dep, ptrs in self.__deps_frozen.items()
            if any(ptr.key == key for ptr in ptrs)
        }

        for dep in deps:
            if dep.key in self.__foofo.keys_to_external_foofos:
                foofo = dep.foofo
                deps.update(foofo.dep_struct.search_dependants(dep.key))

        self.__foofo.print('Searched dependants of \'{}.{}\'', self.__foofo, key)
        self.__dependants_frozen[key] = deps

        return deps

    def __repr__(self):
        return self.__repr


class _AttributePointer:

    def __init__(self,
                 foofo,
                 key):

        if Foofo not in foofo.__class__.__mro__:
            raise Exception

        if key.startswith('_'):
            raise Exception

        self.__foofo = foofo
        self.__key = key
        self.__key_private = '_{}__{}'.format(
            self.__foofo.__class__.__name__,
            self.__key
        )
        self.__repr = '{}.{}'.format(self.__foofo, self.__key)

    @property
    def foofo(self):
        return self.__foofo

    @property
    def key(self):
        return self.__key

    @property
    def key_private(self):
        return self.__key_private

    def get_attr(self):
        return getattr(self.__foofo, self.__key)

    def set_attr(self, val):
        setattr(self.__foofo, self.__key, val)

    def del_attr(self):
        delattr(self.__foofo, self.__key)

    def has_attr(self):
        return hasattr(self.__foofo, self.__key)

    def get_attr_private(self):
        return getattr(self.__foofo, self.__key_private)

    def set_attr_private(self, val):

        # TODO: only allow same class or dormant attribute (with same class)

        setattr(self.__foofo, self.__key_private, val)

    @property
    def is_attr_dormant(self):
        return type(self.get_attr_private()) == _DormantAttribute

    @property
    def is_attr_raw(self):
        return len(self.__foofo.dep_struct[self.__key]) == 0

    def __repr__(self):
        return self.__repr


class _DormantAttribute:

    def __init__(self,
                 foofo_mem_id,
                 attr_name):

        # TODO: record last valid class

        self.__repr = 'Dormant \'{}.{}\''.format(foofo_mem_id, attr_name)

    def __repr__(self):
        return self.__repr
